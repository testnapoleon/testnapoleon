//
//  Strings.swift
//  Posts
//
//  Created by Laddy Diaz Lamus on 6/11/19.
//  Copyright © 2019  All rights reserved.
//

import Foundation

struct Errors {
    static let errorConnection: String = "I am NOT detecting data connection, please check your network."
    static let errorServer: String = "Oops! Something went wrong.\n Feel free to reach us at support@mdcloudps.com, if the issue persists."
    static let errorSecurity: String = "Oops! An error has occurred.\n You will need to restart the App and log back in. We apologize for the inconvenience."
}
