//
//  BannerNavigation.swift
//  Posts
//
//  Created by Laddy Diaz Lamus on 10/11/19.
//  Copyright © 2019  All rights reserved.
//

import UIKit

class BannerNavigation: UIViewController {
    
    @IBOutlet weak var titulo: UILabel!
    @IBOutlet weak var edit: UIButton!
    @IBOutlet weak var deleteAll: UIButton!
    @IBOutlet weak var refresh: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        self.titulo.textColor = Colors.blueNavigationText
        self.view.backgroundColor = Colors.blueNavigationBackground
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
