//
//  PostsTableViewCell.swift
//  Posts
//
//  Created by Laddy Diaz Lamus on 7/11/19.
//  Copyright © 2019  All rights reserved.
//

import UIKit

class PostsTableViewCell: UITableViewCell {
    
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var isRead: UILabel!
    @IBOutlet weak var favoritiesImage: UIImageView!
    
    @IBOutlet weak var separatorView: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    func setCustom(_ row: Post, segmentIndex: Int){
        self.title.text = row.title
        if segmentIndex == 1{
            self.isRead.isHidden = true
            self.favoritiesImage.isHidden = false
        }else{
            self.isRead.isHidden = (row.id <= 20 && row.isRead != true) ? false : true
            self.favoritiesImage.isHidden =  row.isFavoritie != true ? true : false
        }
        self.accessoryType = .disclosureIndicator
        
        self.title.textColor = UIColor.white
        self.backgroundColor = Colors.blueViewBackground
        self.separatorView.backgroundColor = UIColor(red: 58 / 255, green: 82 / 255, blue: 103 / 255, alpha: 1)     //#3A5267
        
    }
}
