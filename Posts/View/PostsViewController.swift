//
//  PostsViewController.swift
//  Posts
//
//  Created by Laddy Diaz Lamus on 7/11/19.
//  Copyright © 2019  All rights reserved.
//

import UIKit

class PostsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var segmentsFavorities: UISegmentedControl!
    
    @IBOutlet weak var messageLabel: UILabel!
    
    
    var dataPost: [Post] = []
    var dataPostFavorities: [Post] = []
    var bannerNavigation = BannerNavigation()
    var refreshControl = UIRefreshControl()

    
    var viewModel: PostsViewModel? {
        
        didSet {
            viewModel?.isPostsValid.dataBinding({ [unowned self] (param) in
                guard let postsValid = param else {
                    return
                }
                if postsValid {
                    if UserDefaultsUtils.getPostsRead().count>0{
                        self.dataPost = UserDefaultsUtils.getPostsRead()
                        self.reloadTableView()
                        
                    }else{
                        if UserDefaultsUtils.getConfirmationDeletesPosts(){
                            self.dataPost =  []
                            
                        }else{
                            self.dataPost = self.viewModel?.dataPost ?? []
                            UserDefaultsUtils.savePosts(posts: self.viewModel?.dataPost ?? [])
                        }
                        
                        
                        self.reloadTableView()
                    }
                    Loading.hide()
                } else {
                    DispatchQueue.main.async { // Correct
                       if self.dataPost.isEmpty{
                           self.messageLabel.isHidden = false
                       }
                    }
                    Loading.hide()
                    
                }
            })
            
            viewModel?.isPostsRefreshValid.dataBinding({ [unowned self] (param) in
                guard let postsRefreshValid = param else {
                    return
                }
                if postsRefreshValid {
                    self.filterRefreshPosts()
                    Loading.hide()
                } else {
                    DispatchQueue.main.async { // Correct
                        if self.dataPost.isEmpty{
                            self.messageLabel.isHidden = false
                        }
                    }
                    Loading.hide()
                }
            })
        }
        
    }
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.viewModel = PostsViewModel()
        self.tableView.delegate = self
        self.tableView.dataSource = self
        tableView.estimatedRowHeight = 30
        tableView.rowHeight = UITableView.automaticDimension
        self.dataPostFavorities = UserDefaultsUtils.getPostsFavorities()
        self.getPosts()
        
        self.customNavigation()
        self.refreshControl.tintColor = UIColor.white
        self.refreshControl.addTarget(self, action: #selector(refresh), for: .valueChanged)
        self.tableView.addSubview(refreshControl)
        
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle{
        return .lightContent
    }
    
    func customNavigation(){
        self.view.backgroundColor =  Colors.blueViewBackground
        self.segmentsFavorities.backgroundColor = Colors.swipeFavorites
        
        self.bannerNavigation.view.frame = CGRect(x: 0, y: 0, width: (self.navigationController?.navigationBar.frame.size.width)!, height: (self.navigationController?.navigationBar.frame.size.height)!)
        self.navigationController?.navigationBar.addSubview(bannerNavigation.view)
        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.navigationBar.barTintColor = Colors.blueNavigationBackground
        self.bannerNavigation.refresh.addTarget(self, action: #selector(refresh), for: .touchUpInside)
        self.bannerNavigation.edit.addTarget(self, action: #selector(edit), for: .touchUpInside)
        self.bannerNavigation.deleteAll.addTarget(self, action: #selector(deleteAll), for: .touchUpInside)
        
    }
    
    @objc func edit() {
        
        self.tableView.isEditing = !self.tableView.isEditing ? true : false
        self.navigationItem.rightBarButtonItem?.title = !self.tableView.isEditing ? "Edit" : "Done"
        
    }
    
    @objc func refresh() {
        self.viewModel?.getResfreshPosts()
        self.refreshControl.endRefreshing()
        
    }
    
    @objc func getPosts() {
        self.viewModel?.getPosts()
        self.refreshControl.endRefreshing()
        
    }
    
    @objc func deleteAll() {
        if self.dataPost.count>0{
            let alertMenu = UIAlertController(title: "WARNING", message:  "Do you want to DELETE alls Post?.", preferredStyle: .alert)
            let ok = UIAlertAction(title: "YES", style: .destructive, handler: {
                (alert: UIAlertAction!) -> Void in
                Loading.show()
                UserDefaultsUtils.saveConfirmationDeletesPosts(Posts: true)
                self.dataPost = []
                UserDefaultsUtils.savePosts(posts: [])
                self.dataPostFavorities = []
                UserDefaultsUtils.saveFavorities(posts: [])
                self.tableView.isEditing = false
                self.tableView.reloadData()
                Loading.hide()
                
            })
            
            let destroyAction = UIAlertAction(title: "GO BACK", style: .default) { (action) in  }
            alertMenu.addAction(destroyAction)
            alertMenu.addAction(ok)
            alertMenu.view.tintColor = Colors.green_7CB230
            self.present(alertMenu, animated: true, completion: nil)
        }else{
            let alertMenu = UIAlertController(title: "ATTENTION", message:  "There is no post to delete", preferredStyle: .alert)
            let ok = UIAlertAction(title: "OK", style: .default, handler: { (alert: UIAlertAction!) -> Void in })
            alertMenu.addAction(ok)
            self.present(alertMenu, animated: true, completion: nil)
        }
    }
    
    
    // filterRefreshPosts:
    // 1. SI EL USUARIO CONFIRMO QUE ELIMINO TODOS LOS POSTS, AL HACER REFRES SE DEBE FILTRAR TODOS LOS QUE ME MUESTRA LA API, DE LO CONTRARIO SIGUE CON EL SEGUNDO ITEM--->
    // 2. FILTRA LOS POST QUE NO ESTAN ELIMINADOS CON LOS DE LA API, SE TOMA COMO IDENTIFICADOR EL ID Y SE REEMPLAZA EL USER_ID, TITLE Y BODY POR EL QUE SE TRAE EL API, LOS DEMAS CAMPOS OPCIONALES COMO EL isRead, isFavoritie, isdelete NO SERAN REEMPLAZADOS
    
    func filterRefreshPosts(){
        if UserDefaultsUtils.getConfirmationDeletesPosts(){
            UserDefaultsUtils.saveConfirmationDeletesPosts(Posts: false)
            self.dataPost = self.viewModel?.dataPostRefresh ?? []
        }else{
            var data : [Post] = []
            if self.dataPost.isEmpty{
                self.dataPost = UserDefaultsUtils.getPostsRead()
            }
            for row in self.dataPost{
                let dataResult = self.viewModel!.dataPostRefresh.filter({$0.id == row.id})
                if dataResult.count>0{
                    data.append(Post(id: row.id, userId: dataResult[0].userId, title: dataResult[0].title, body: dataResult[0].body, isRead: row.isRead ?? false, isFavoritie: row.isFavoritie ?? false, isDelete: row.isDelete ?? false))
                }
            }
            self.dataPost = data
        }
        
        self.reloadTableView()
        
    }
    
    
    @IBAction func favotiriesAction(_ sender: UISegmentedControl) {
        self.tableView.reloadData()
        
    }
    
    func reloadTableView(){
        DispatchQueue.main.async { // Correct
           self.tableView.reloadData()
           self.messageLabel.isHidden = true
            
        }
        
//        let qos = DispatchQoS(qosClass: .userInitiated, relativePriority: 0)
//        DispatchQueue.global(qos: qos.qosClass).async(execute: {
//            DispatchQueue.main.async(execute: {
//                self.tableView.reloadData()
//                self.messageLabel.isHidden = true
//            })
//        })
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return  self.segmentsFavorities.selectedSegmentIndex == 0 ? self.dataPost.count : self.dataPostFavorities.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell = tableView.dequeueReusableCell(withIdentifier: "PostsTableViewCell") as? PostsTableViewCell
        if (cell == nil) {
            tableView.register(UINib(nibName: "PostsTableViewCell", bundle: nil), forCellReuseIdentifier: "PostsTableViewCell")
            cell = tableView.dequeueReusableCell(withIdentifier: "PostsTableViewCell") as? PostsTableViewCell
        }
        let row = self.segmentsFavorities.selectedSegmentIndex == 0 ? self.dataPost[indexPath.row] : self.dataPostFavorities[indexPath.row]
        cell?.setCustom(row, segmentIndex: self.segmentsFavorities.selectedSegmentIndex)
        
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let row = self.segmentsFavorities.selectedSegmentIndex == 0 ? self.dataPost[indexPath.row] : self.dataPostFavorities[indexPath.row]
        if self.segmentsFavorities.selectedSegmentIndex == 0{
            self.dataPost[indexPath.row].isRead = true
        }
        UserDefaultsUtils.savePosts(posts: self.dataPost)
        let vc = PostDetailViewController()
        vc.initWith(titlePost: row.title, body: row.body, user: row.userId)
        let navController = UINavigationController(rootViewController: vc)
        navController.modalPresentationStyle = .fullScreen
        self.present(navController, animated: true, completion: nil)
        self.tableView.reloadData()
        
        
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    
    func tableView(_ tableView: UITableView, leadingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        let closeAction = UIContextualAction(style: .normal, title:  "Favorites", handler: { (ac:UIContextualAction, view:UIView, success:(Bool) -> Void) in
            let row =  self.dataPost[indexPath.row] 
            
            
            let data = self.dataPostFavorities.filter({$0.id == row.id})
            if data.isEmpty{
                self.dataPostFavorities.append(row)
                self.dataPost[indexPath.row].isFavoritie = true
                self.tableView.reloadData()
                UserDefaultsUtils.saveFavorities(posts: self.dataPostFavorities)
                UserDefaultsUtils.savePosts(posts: self.dataPost)
            }
            success(true)
        })
        closeAction.backgroundColor = .purple
        
        return UISwipeActionsConfiguration(actions: [closeAction])
        
    }
    
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        
        let DeleteAction = UIContextualAction(style: .destructive, title: "Delete") { (action, view, handler) in
            self.dataPost.remove(at: indexPath.row)
            self.dataPost[indexPath.row].isDelete = true
            UserDefaultsUtils.savePosts(posts: self.dataPost)
            self.tableView.deleteRows(at: [IndexPath(row: indexPath.row, section: 0)], with: .fade)
        }
        
        DeleteAction.backgroundColor = Colors.swipeDelete
        let configuration = UISwipeActionsConfiguration(actions: [DeleteAction])
        configuration.performsFirstActionWithFullSwipe = false
        return configuration
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return self.segmentsFavorities.selectedSegmentIndex == 0 ? true : false
        
    }
    
}


