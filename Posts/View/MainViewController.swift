//
//  MainViewController.swift
//  Posts
//
//  Created by Laddy Diaz Lamus on 7/11/19.
//  Copyright © 2019  All rights reserved.
//

import UIKit

class MainViewController: UIViewController {
    
    @IBOutlet weak var logoImage: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        UIView.animate(withDuration: 1.3, animations: {
            
            let screenSize: CGRect = UIScreen.main.bounds
            let screenHeight = screenSize.height
            if UIScreen.main.nativeBounds.height == 2436 {
                self.logoImage.frame.origin.y -= (screenHeight - 284)
            } else {
                self.logoImage.frame.origin.y -= (screenHeight - 250)
            }
        }, completion: { finished in
            
            
            let story = UIStoryboard(name: "TabBar", bundle: nil)
            let vc = story.instantiateViewController(identifier: "TabbarViewController")
            vc.modalPresentationStyle = .fullScreen
            self.present(vc, animated: false, completion: nil)
            
            
        })
    }
}



