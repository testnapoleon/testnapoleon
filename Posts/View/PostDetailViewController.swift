//
//  PostDetailViewController.swift
//  Posts
//
//  Created by Laddy Diaz Lamus on 8/11/19.
//  Copyright © 2019  All rights reserved.
//

import UIKit

class PostDetailViewController: UIViewController {
    
    
    @IBOutlet weak var userId: UILabel!
    @IBOutlet weak var postTitle: UILabel!
    @IBOutlet weak var body: UITextView!
    
    var titlePost = ""
    var bodyPost = ""
    var user = 0
    
    func initWith(titlePost: String, body: String, user:Int){
        self.titlePost = titlePost
        self.bodyPost = body
        self.user = user
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.postTitle.text = self.titlePost
        self.body.text = self.bodyPost
        self.userId.text = "User id: \(self.user)"
        self.view.backgroundColor = Colors.blueViewBackground
        self.userId.textColor = .white
        self.postTitle.textColor = .white
        self.body.textColor = .white
        
        self.customNavigation()
        
        let backButton = UIBarButtonItem(title: "Close", style: UIBarButtonItem.Style.plain, target: self, action: #selector(cancel))
        navigationController?.navigationBar.topItem?.leftBarButtonItem = backButton
        
    }
    
    func customNavigation(){
        let nav = self.navigationController?.navigationBar
        nav?.barTintColor = Colors.blueViewBackground
        nav?.tintColor = Colors.blueNavigationText
        
    }
    
    @objc func cancel(){
        self.dismiss(animated: true, completion: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = false  
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle{
        return .lightContent
    }
    
}

