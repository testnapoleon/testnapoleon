//
//  Post.swift
//  Posts
//
//  Created by Laddy Diaz Lamus on 6/11/19.
//  Copyright © 2019  All rights reserved.
//

import Foundation

struct Post: Decodable, Encodable {
    
    var id: Int
    var userId: Int
    var title: String
    var body: String
    var isRead: Bool?
    var isFavoritie: Bool?
    var isDelete: Bool?

    
    

    
    private enum CodingKeys: CodingKey {
        case id
        case userId
        case title
        case body
        case isRead
        case isFavoritie
        case isDelete
        
    }
    
    init(id: Int, userId: Int, title: String, body: String, isRead: Bool, isFavoritie: Bool, isDelete: Bool ) {
        self.id = id
        self.userId = userId
        self.title = title
        self.body = body
        self.isRead = isRead
        self.isFavoritie = isFavoritie
        self.isDelete = isDelete


    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.id = try container.decode(Int.self, forKey: .id)
        self.userId = try container.decode(Int.self, forKey: .userId)
        self.title = try container.decode(String.self, forKey: .title)
        self.body = try container.decode(String.self, forKey: .body)
        self.isRead = try container.decodeIfPresent(Bool.self, forKey: .isRead)
        self.isFavoritie = try container.decodeIfPresent(Bool.self, forKey: .isFavoritie)
        self.isDelete = try container.decodeIfPresent(Bool.self, forKey: .isDelete)


    }
}
