//
//  ServerResponse.swift
//  Posts
//
//  Created by Laddy Diaz Lamus on 6/11/19.
//  Copyright © 2019  All rights reserved.
//

import Foundation

struct ServerResponse<T: Decodable>: Decodable {
    let status: Bool
    var data: T?
    let error: Error?
}

struct Error: Decodable {
    let code: String
    let message: String
    let description: String?
    
    init(code: String, message: String) {
        self.code = code
        self.message = message
        self.description = ""
    }
}
