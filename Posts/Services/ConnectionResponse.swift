//
//  ConnectionResponse.swift
//  Posts
//
//  Created by Laddy Diaz Lamus on 6/11/19.
//  Copyright © 2019  All rights reserved.
//

import Foundation

enum ConnectionResponse {
    case success(result: Data)
    case failure
    case responseUnsuccessful(code: Int)
    case invalidData
}
