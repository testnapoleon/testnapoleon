//
//  ModelResponse.swift
//  Posts
//
//  Created by Laddy Diaz Lamus on 6/11/19.
//  Copyright © 2019  All rights reserved.
//

import Foundation

enum ModelResponse<T> {
    case success(result: T)
    case error(result: Error)
}
