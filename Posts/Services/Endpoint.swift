//
//  Endpoint.swift
//  Posts
//
//  Created by Laddy Diaz Lamus on 6/11/19.
//  Copyright © 2019  All rights reserved.
//

import Foundation

protocol Endpoint {
    var base: String { get }
    var path: String { get }
}
