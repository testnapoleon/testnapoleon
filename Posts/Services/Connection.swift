//
//  Connection.swift
//  Posts
//
//  Created by Laddy Diaz Lamus on 6/11/19.
//  Copyright © 2019  All rights reserved.
//

import Foundation
  
class Connection {
    
    static var DOMAIN: String?
    static var TOKEN: String?
    static var TOKEN_TEX: String?
    
    
    static func send(endpoint: Endpoint, completion: @escaping (ConnectionResponse) -> Void) {
        let url = URL(string: "\(Constants.protocol)\(endpoint.base)\(endpoint.path)")
        let request: NSMutableURLRequest = NSMutableURLRequest(url: url!)
        request.httpMethod = "GET"
        request.timeoutInterval = 8
        let session = URLSession.shared
        let task = session.dataTask(with: request as URLRequest, completionHandler: {data, response, error -> Void in
            
            if error == nil {
                let httpResponse = response as! HTTPURLResponse
                if httpResponse.statusCode == 200 {
                    if data!.count > 0 {
                        completion(.success(result: data!))
                    } else {
                        completion(.invalidData)
                    }
                } else {
                    completion(.responseUnsuccessful(code: httpResponse.statusCode))
                }
            } else {
                completion(.failure)
            }
            
            
        })
        task.resume()
    }
    
}
