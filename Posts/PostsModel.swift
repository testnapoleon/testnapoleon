//
//  PostsModel.swift
//  Posts
//
//  Created by Laddy Diaz Lamus on 6/11/19.
//  Copyright © 2019  All rights reserved.
//

import Foundation

enum PostsAction {
    case getPosts
    case getPosts1
    case getPostsComments
    case getPostsUserId1

    
}

extension PostsAction: Endpoint {
    
    var base: String {
        return ""
    }
    
    var path: String {
        switch self {
            //https://
            case .getPosts: return "jsonplaceholder.typicode.com/posts"
            case .getPosts1: return "jsonplaceholder.typicode.com/posts/1"
            case .getPostsComments: return "jsonplaceholder.typicode.com/posts/1/comments"
            case .getPostsUserId1: return "jsonplaceholder.typicode.com/posts?userId=1"

            
       
        }
    }
}

class PostsModel {
    
    func getPosts( completion: @escaping (_ response: ModelResponse<[Post]>) -> Void) {
        
       
        Connection.send(endpoint: PostsAction.getPosts) { (response) in
            switch response {
            case .success(let result):
                do {
                    let decoded = try JSONDecoder().decode([Post].self, from: result)
                    if decoded.count>0{
                        completion(.success(result: decoded))

                    }
                } catch {
                    completion(.error(result: Error.init(code: "CD4001", message: Errors.errorServer)))
                }
                break
                
            case .failure:
                completion(.error(result: Error.init(code: "CD4000", message: Errors.errorConnection)))
                break
                
            case .responseUnsuccessful(let code):
                if code == 412 {
                    completion(.error(result: Error.init(code: "CD4002", message: Errors.errorSecurity)))
                } else {
                    completion(.error(result: Error.init(code: "CD4003", message: Errors.errorServer)))
                }
                break
                
            case .invalidData:
                completion(.error(result: Error.init(code: "CD4001", message: Errors.errorServer)))
                break
            }
        }
    }
}

