//
//  PostsViewModel.swift
//  Posts
//
//  Created by Laddy Diaz Lamus on 6/11/19.
//  Copyright © 2019  All rights reserved.
//

import UIKit

protocol PostsViewModelProtocol {
    var isPostsValid: Binding<Bool?> { get }
    var isPostsRefreshValid: Binding<Bool?> { get }
    
    func getPosts()
}

class PostsViewModel: NSObject, PostsViewModelProtocol {
    let model = PostsModel()
    
    var isPostsValid: Binding<Bool?>
    var isPostsRefreshValid: Binding<Bool?>
    var dataPost: [Post] = []
    var dataPostRefresh: [Post] = []
    
    var messageError: String = ""
    var theEmail: String = ""
    
    func getPosts() {
        Loading.show()
        self.model.getPosts { [weak self] response in
            guard let _ = self else {
                return
            }
            
            switch response {
                
            case .success(let result):
                self?.dataPost = result
                self?.isPostsValid.value = true
                
            case .error(let error):                
                self?.messageError = error.message + " " + error.code
                self?.isPostsValid.value = false
                break
            }
        }
    }
    
    func getResfreshPosts() {
        Loading.show()
        self.model.getPosts { [weak self] response in
            guard let _ = self else {
                return
            }
            
            switch response {
                
            case .success(let result):
                self?.dataPostRefresh = result
                self?.isPostsRefreshValid.value = true
            case .error(let error):                
                self?.messageError = error.message + " " + error.code
                self?.isPostsRefreshValid.value = false
                break
            }
        }
    }
    
    
    
    
    
    
    override init() {
        self.isPostsValid = Binding(nil)
        self.isPostsRefreshValid = Binding(nil)
        
    }
}
