//
//  Binding.swift
//  Posts
//
//  Created by Laddy Diaz Lamus on 6/11/19.
//  Copyright © 2019  All rights reserved.
//

import Foundation

class Binding<T> {
    typealias Listener = (T) -> ()
    var listener: Listener?
    
    func dataBinding(_ listener: Listener?) {
        self.listener = listener
        listener?(value)
    }
    
    var value: T {
        didSet {
            listener?(value)
        }
    }
    
    init(_ v: T) {
        value = v
    }
}

