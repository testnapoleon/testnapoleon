//
//  Colors.swift
//  Posts
//
//  Created by Laddy Diaz Lamus on 8/11/19.
//  Copyright © 2019  All rights reserved.
//

import Foundation
import UIKit

struct Colors {

    static let swipeFavorites = UIColor(red: 105 / 255, green: 163 / 255, blue: 245 / 255, alpha: 1)
    static let swipeDelete = UIColor(red: 235 / 255, green: 2 / 255, blue: 64 / 255, alpha: 1)
    
    static let green_7CB230 = UIColor(red: 124 / 255, green: 178 / 255, blue: 48 / 255, alpha: 1)   //#7CB230
    
    static let blueNavigationText = UIColor(red: 139 / 255, green: 201 / 255, blue: 255 / 255, alpha: 1) 
    static let blueNavigationBackground = UIColor(red: 63 / 255, green: 75 / 255, blue: 106 / 255, alpha: 1) 
    static let blueViewBackground = UIColor(red: 27 / 255, green: 44 / 255, blue: 84 / 255, alpha: 1)





}
