//
//  UserDefaultsUtils.swift
//  Posts
//
//  Created by Laddy Diaz Lamus on 8/11/19.
//  Copyright © 2019  All rights reserved.
//

import Foundation

struct UserDefaultsUtils {
    
    fileprivate static var ud: UserDefaults {
        return UserDefaults.init()
    }
    
    /** PostsRead **/
    static func getPostsRead() -> [Post] {
        if let data = ud.object(forKey: "postsRead") as? Data {
            do{
                return try PropertyListDecoder().decode(Array<Post>.self, from: data)
            }catch{
                return []
            }
        }
        
        return []
    }
    
    static func savePosts(posts: [Post]) {
        ud.set(try? PropertyListEncoder().encode(posts), forKey: "postsRead")
        ud.synchronize()
    }
    
    /** End **/
    
    
    /** PostsFavorities **/
    static func getPostsFavorities() -> [Post] {
        if let data = ud.object(forKey: "postsFavorities") as? Data {
            do{
                return try PropertyListDecoder().decode(Array<Post>.self, from: data)
            }catch{
                return []
            }
        }
        
        return []
    }
    
    static func saveFavorities(posts: [Post]) {
        ud.set(try? PropertyListEncoder().encode(posts), forKey: "postsFavorities")
        ud.synchronize()
    }
    
    /** End **/
    
    
    /** ConfirmationDeletesPosts **/
    static func saveConfirmationDeletesPosts(Posts: Bool) {
        ud.set(Posts, forKey: "confirmationPostsDeletes")
        ud.synchronize()
    }
    
    static func getConfirmationDeletesPosts() -> Bool {
        if let data = ud.object(forKey: "confirmationPostsDeletes") as? Bool {
            return data
        }
        return false
    }
    
    /** End **/
}
